﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNet.SignalR;

namespace RealTimeSignalRServer
{
    public class AnalyticsHub : BaseHub
    {
        private static readonly ConcurrentBag<Tuple<DateTime, string, string>> Messages = new ConcurrentBag<Tuple<DateTime, string, string>>();
        private static readonly ConcurrentDictionary <string, UserMessagePayload> HubUsers = new ConcurrentDictionary<string, UserMessagePayload>();

        //private static readonly JsonSerializerSettings JsonSerializationSettings = new JsonSerializerSettings
        //    {
        //        ContractResolver = new CamelCasePropertyNamesContractResolver(),
        //        Formatting = Formatting.None
        //    };

        private static IEnumerable<UserMessagePayload> Users
        {
            get
            {
                return HubUsers.Values;
            }
        }

        public override Task OnConnected()
        {
            var user = this.CreateUser();
            HubUsers.TryAdd(Context.ConnectionId, user);
            this.BroadcastUserAdded(user);
            BroadcastUpdates();
            return base.OnConnected();
        }

        public override Task OnDisconnected()
        {
            var user = HubUsers[Context.ConnectionId];
            user.DisconnectedAt = DateTime.UtcNow;
            this.BroadcastUserRemoved(user);
            BroadcastUpdates();
            return base.OnDisconnected();
        }

        public void Send(string message)
        {
            var data = new Tuple<DateTime, string, string>(DateTime.UtcNow, Context.ConnectionId, message);
            Messages.Add(data);
            this.BroadcastData(data);
        }

        internal static IEnumerable<Tuple<DateTime, string, string>> GetMessages()
        {
            return Messages.ToList();
        }

        internal static void BroadcastUsers(dynamic caller)
        {
            var users = new List<UserMessagePayload>(AnalyticsHub.Users);
            caller.Message(new HubMessage { Type = "users", Message = users.OrderByDescending(u => u.ConnectedAt).Take(100) });
        }

        internal static void BroadcastUserSummary(dynamic caller)
        {
            var users = new List<UserMessagePayload>(AnalyticsHub.Users);
            var activeUsersCount = users.Count(u => u.DisconnectedAt == null);
            var inactiveUsersCount = users.Count - activeUsersCount;
            var connectionStartTimes = users.Select(u => u.ConnectedAt).OrderBy(d => d);
            var firstConnectionStart = connectionStartTimes.FirstOrDefault();
            var lastConnectionStart = connectionStartTimes.LastOrDefault();
            var messages = AnalyticsHub.GetMessages();
            var lastMessage = messages.LastOrDefault();
            var ativitiesSpan =
                users.Where(u => u.DisconnectedAt != null).Select(u => (int)(u.DisconnectedAt - u.ConnectedAt).Value.TotalMilliseconds).ToList();
            var minTime = ativitiesSpan.Any() ? ativitiesSpan.Min() : 0;
            var maxTime = ativitiesSpan.Any() ? ativitiesSpan.Max() : 0;
            var avgActivity = ativitiesSpan.Any() ? (int)ativitiesSpan.Average() : 0;
            var medianActivity = Median(ativitiesSpan);
            //var message =
            //    JsonConvert.SerializeObject(
            //        new
            //            {
            //                ActiveUsers = activeUsersCount,
            //                InactiveUsers = inactiveUsersCount,
            //                FirstUserConnectedAt =
            //                    firstConnectionStart != default(DateTime) ? firstConnectionStart : (DateTime?)null,
            //                LastUserConnectedAt =
            //                    lastConnectionStart != default(DateTime) ? lastConnectionStart : (DateTime?)null,
            //                LastUserActivityAt = lastMessage != null ? lastMessage.Item1 : (DateTime?)null,
            //                LongestActivityMinutes = maxTime,
            //                ShortestActivityMinutes = minTime,
            //                AverageActivityMinutes = avgActivity,
            //                MedianActivityMinutes = medianActivity
            //            },
            //        JsonSerializationSettings);
            //var fixedMessage = message.Replace("\"", string.Empty);

            caller.Message(new HubMessage
            {
                Type = "summary",
                Message = new SummaryMessagePayload
                        {
                            ActiveUsers = activeUsersCount,
                            InactiveUsers = inactiveUsersCount,
                            FirstUserConnectedAt =
                                firstConnectionStart != default(DateTime) ? firstConnectionStart : (DateTime?)null,
                            LastUserConnectedAt =
                                lastConnectionStart != default(DateTime) ? lastConnectionStart : (DateTime?)null,
                            LastUserActivityAt = lastMessage != null ? lastMessage.Item1 : (DateTime?)null,
                            LongestActivityMinutes = maxTime,
                            ShortestActivityMinutes = minTime,
                            AverageActivityMinutes = avgActivity,
                            MedianActivityMinutes = medianActivity
                        }
            });
        }

        private static dynamic GetDahsboradHubClients()
        {
            return GlobalHost.ConnectionManager.GetHubContext<DashboardHub>().Clients.All;
        }

        private static void BroadcastUpdates()
        {
            BroadcastUserSummary(GetDahsboradHubClients());
        }

        private static int Median(List<int> source)
        {
            if (!source.Any())
            {
                return 0;
            }

            var sortedList = from number in source
                             orderby number
                             select number;

            int itemIndex = (int)sortedList.Count() / 2;

            if (sortedList.Count() % 2 == 0)
            {
                // Even number of items. 
                return (sortedList.ElementAt(itemIndex) + sortedList.ElementAt(itemIndex - 1)) / 2;
            }
            else
            {
                // Odd number of items. 
                return sortedList.ElementAt(itemIndex);
            }
        }

        private void BroadcastData(Tuple<DateTime, string, string> data)
        {
            //var message = JsonConvert.SerializeObject(new { timestamp = reqUsers });
            GetDahsboradHubClients().Message(new HubMessage() { Type = "data", Message = new DataMessagePayload { Id = data.Item2, Message = data.Item3, Timestamp = data.Item1 } });
        }

        private void BroadcastUserAdded(UserMessagePayload user)
        {
            GetDahsboradHubClients().Message(new HubMessage() { Type = "userAdded", Message = user });
        }

        private void BroadcastUserRemoved(UserMessagePayload user)
        {
            GetDahsboradHubClients().Message(new HubMessage { Type = "userRemoved", Message = user });
        }
    }
}