﻿namespace RealTimeSignalRServer
{
    using System;

    using Newtonsoft.Json;

    public class UserMessagePayload
    {
        [JsonProperty("connectedAt")]
        public DateTime ConnectedAt { get; set; }

        [JsonProperty("disconnectedAt")]
        public DateTime? DisconnectedAt { get; set; }

        [JsonProperty("connectedFrom")]
        public string ConnectedFrom { get; set; }

        [JsonProperty("connectionId")]
        public string ConnectionId { get; set; }

        [JsonProperty("userId")]
        public string UserId { get; set; }
    }
}