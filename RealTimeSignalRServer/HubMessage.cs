﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealTimeSignalRServer
{
    using Newtonsoft.Json;

    public class HubMessage
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("message")]
        public dynamic Message { get; set; }
    }
}