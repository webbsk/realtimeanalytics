﻿namespace RealTimeSignalRServer
{
    using System.Threading.Tasks;

    public class DashboardHub : BaseHub
    {
        public override Task OnConnected()
        {
            AnalyticsHub.BroadcastUsers(Clients.Caller);
            AnalyticsHub.BroadcastUserSummary(Clients.Caller);
            return base.OnConnected();
        }
    }
}