﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RTDashboard.Controllers
{
    public class DashboardController : Controller
    {
        //
        // GET: /Dashboard/
        public ActionResult Index(string theme, string id)
        {
            ViewBag.theme = theme;
            ViewBag.id = string.IsNullOrWhiteSpace(id) ? string.Empty : id;
            return View();
        }

    }
}
